<?php

namespace Search\Providers;

use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use Illuminate\Support\ServiceProvider AS Provider;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\AliasLoader;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class ServiceProvider extends Provider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/search'),
        ], 'views_search');
        $this->publishes([
            __DIR__.'/../../resources/js' => public_path('resources/js/search'),
        ], 'script_search');
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'search');
        $this->loadViewsFrom(__DIR__.'/../../resources/components', 'gallery');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
//        $this->components();
    }

//    private function components()
//    {
//        Blade::component(Images::class, 'gallery::images');
//        Blade::component(Category::class, 'gallery::category');
//    }
//
//    private function singletons()
//    {
//        $this->app->singleton(Images::class, function ($app, $parameters) {
//            $categoryId = $parameters['categoryId'];
//            return new Images($categoryId);
//        });
//        $this->app->singleton(Category::class, function ($app) {
//            $categoryBuilder = $app->make(GalleryCategoryBuilder::class);
//            return new Category($categoryBuilder);
//        });
//
//    }
    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind(QueryBuilder::class, GalleryCategoryBuilder::class);
        $this->mergeConfigFrom(__DIR__.'/../../config/search.php', 'search');
//        $this->singletons();
    }
}