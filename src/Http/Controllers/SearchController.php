<?php

namespace Search\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin\Page\PageCreate;
use App\Models\Admin\PhotoGallery\GalleryCategory;
use Catalog\QueryBuilder\CatalogBuilder;
use Gallery\QueryBuilder\GalleryCategoryBuilder;
use Illuminate\Http\Request;
use Search\QueryBuilder\SearchBuilder;
use Search\Requests\SearchRequest;

class SearchController extends Controller
{
    public function index(SearchBuilder $builder)
    {
        return \view('search::search.index',[
            'breadcrumbs' => $builder->getBreadcrumb(),
        ]);
    }

    public function search(SearchRequest $request)
    {
        $builder = config('search.builder');
        $method = config('search.method');
        $builder = new $builder();
        return $builder->$method($request->text);
    }
}
