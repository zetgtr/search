<?php

namespace Search\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'text' => 'required'
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}