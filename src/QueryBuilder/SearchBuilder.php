<?php

namespace Search\QueryBuilder;

use App\Models\Admin\Page\PageCreate;
use App\Models\Admin\Panel\DataPanel;
use App\Models\Admin\Panel\Panel;
use App\Models\Admin\PhotoGallery\GalleryCategory;
use App\QueryBuilder\Admin\Page\PageBuilder;
use App\QueryBuilder\QueryBuilder;
use Document\Models\Document;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use News\Models\News;

class SearchBuilder extends QueryBuilder
{
    public function getBreadcrumb(){
        return [['title'=>'Главная','url'=>'/'],['title'=>config('search.title')]];
    }
    public function getAll(): Collection
    {
        //
    }

    public function search($text)
    {
        $pages = PageCreate::search($text)->get();
        $pagesData = DataPanel::search($text)->get();
        $news = News::where('title','LIKE','%'.$text.'%')
            ->whereOr('description','LIKE','%'.$text.'%')
            ->whereOr('content','LIKE','%'.$text.'%')->get();
        $documents = Document::where('title','LIKE','%'.$text.'%')
            ->whereOr('description','LIKE','%'.$text.'%')->get();
        $panels = Panel::search($text)->get();
        $pagesCollection = collect();
        $this->setCollect($pagesCollection,$panels,'pages');
        $this->setCollect($pagesCollection,$pages);
        $this->setCollect($pagesCollection,$pagesData,'page',true);
        foreach ($pagesCollection as $key=>$page) {
            if ($page)
                $page->url = $this->setUrlPage($page);
            else
                unset($pagesCollection[$key]);
        }

        return ['pages'=>$pagesCollection->values()->toArray(),'news'=>$news,'documents'=>$documents];
    }

    public function setCollect(&$collection,$items,$method = false,$check = false){

        foreach ($items as $item){
            if($method) {
                if($check)
                    $collection = $collection->push($item->$method);
                else
                    $collection = $collection->merge($item->$method);
            }else{
                $collection = $collection->push($item);
            }
        }

        $collection = $collection->unique('id');
    }

    private function setUrlPage($page,$url=""){
        if($url)
            $url = $page->url . "/" . $url;
        else
            $url = $page->url;
        if ($page->parentPage) {
            if (is_object($page->parentPage)) {
                $url = $this->setUrlPage($page->parentPage, $url);
            }
        }

        return $url;
    }
}
