<?php

use Illuminate\Support\Facades\Route;
use \Search\Http\Controllers\SearchController;


Route::middleware('web')->group(function (){
    Route::get('/'.config('search.url'),[SearchController::class,'index'])->name('search');
    Route::post('/'.config('search.url'),[SearchController::class,'search'])->name('search.post');
});
