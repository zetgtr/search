<?php

use Illuminate\Support\Facades\Route;
use \Gallery\Http\Controllers\PhotoGalleryController;

Route::middleware('api')->group(function (){
    Route::middleware('is_admin')->group(function () {
        Route::group(['prefix' => 'admin', 'as' => 'admin.'], static function() {
            Route::post('get_gallery_category',[PhotoGalleryController::class,'getCategory']);
            Route::post('get_gallery_image',[PhotoGalleryController::class,'getImage']);
            Route::post('gallery/set_order_category',[PhotoGalleryController::class,'setOrderCategory']);
            Route::post('gallery/set_order_image',[PhotoGalleryController::class,'setOrderImage']);
        });
    });
});
