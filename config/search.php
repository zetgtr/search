<?php

use Laravel\Fortify\Features;

return [
    'title' => 'Поиск',
    'url' => 'search',
    'builder' => 'Search\\QueryBuilder\\SearchBuilder',
    'method' => 'search'
];

