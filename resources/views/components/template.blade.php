<template id="item_search">
    <div class="search__item">
        <a href="" class="item__link" target="_blank">
            <div class="search__text">
                <div class="info"></div>
                <div class="title"></div>
            </div>
        </a>
    </div>
</template>
