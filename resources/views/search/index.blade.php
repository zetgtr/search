@extends('layouts.inner')
@section('title', config('search.title'))
@section('content')
<div class="content">
    <x-front.breadcrumbs :breadcrumbs="$breadcrumbs" flag='true' />
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Поиск по сайту</h2>
                <div class="form-group search__form">
                    {{-- <label class="search__label">Введите строку для поиска</label> --}}
                    <input type="text" class="form-control search_input" placeholder="Введите строку для поиска">
                </div>
            </div>
            <div class="search_container">
            </div>
        </div>
    </div>
</div>
</div>
<x-search::template />

<input type="hidden" id="route_search" value="{{ route('search.post') }}">
@vite('resources/js/search/search.js')
@endsection
