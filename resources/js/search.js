class Search {
    constructor() {
        this.input = document.querySelector('.search_input')
        this.route = document.getElementById('route_search').value
        this.template = document.getElementById('item_search').content.cloneNode(true).firstElementChild
        this.searchContainer = document.querySelector(".search_container");
        this.addEvent()
    }

    addEvent() {
        this.input.addEventListener('input', this.search.bind(this))
    }

    search(e) {
        const text = e.target.value
        if (text.length > 2)
            axios.post(this.route, { text }).then(this.setData.bind(this))


    }

    setData({ data }) {
        this.searchContainer.innerHTML = ''

        if (data.news && data.news.length > 0) {
            this.setDataHtml(data.news, 'Новости')
        }
        if (data.pages && data.pages.length > 0) {
            this.setDataHtml(data.pages, 'Страницы')
        }
        if (data.documents && data.documents.length > 0) {
            this.setDataHtml(data.documents, 'Документы')
        }
    }

    checkData(text,el){
        switch (text) {
            case 'Новости':
                this.newTemplate.querySelector(".item__link").href = "news/" + el.url;
                break;
            case "Страницы":
                this.newTemplate.querySelector(".item__link").href = el.url;
                break;
            case "Документы":
                this.newTemplate.querySelector(".item__link").href = el.link;
                break;
        }
    }

    setDataHtml(data, text) {
        const elementsToAdd = [];

        data.forEach(el => {
            this.newTemplate = this.template.cloneNode(true);
            const title = this.newTemplate.querySelector(".search__text .title")
            const info = this.newTemplate.querySelector(".search__text .info")
            if (el.url || el.link) {
                this.checkData(text,el)
            }

            if (el.title) {
                title.innerText = el.title;
            }

            info.innerText = text + ":";
            elementsToAdd.push(this.newTemplate);
        });

        elementsToAdd.forEach(element => {
            this.searchContainer.appendChild(element);
        });
    }
}

$(document).ready(() => {
    new Search()
})
